import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';

import { MaterialExampleModule } from '../material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { HeaderComponent } from './component/header/header.component';
import { SidebarComponent } from './component/sidebar/sidebar.component';
import { FooterComponent } from './component/footer/footer.component';
import { DefaultComponent } from './component/default/default.component';
import { ContentComponent } from './component/content/content.component';
import { MatNativeDateModule } from '@angular/material/core';
import { SubjectsService } from './subjects.service';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { DialogUpdateComponent } from './component/dialog-update/dialog-update.component';
import { DialogDelateComponent } from './component/dialog-delate/dialog-delate.component';
import { FormSubjectComponent } from './component/form-subject/form-subject.component';
import { StylePaginatorDirective } from './style-paginator.directive';
import { DialogSucceedComponent } from './component/dialog-succeed/dialog-succeed.component';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    SidebarComponent,
    FooterComponent,
    DefaultComponent,
    ContentComponent,
    DialogUpdateComponent,
    DialogDelateComponent,
    FormSubjectComponent,
    StylePaginatorDirective,
    DialogSucceedComponent,

  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MatNativeDateModule,
    MaterialExampleModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    HttpClientModule
  ],
  entryComponents: [ContentComponent],
  providers: [SubjectsService],
  bootstrap: [AppComponent]
})
export class AppModule { }
platformBrowserDynamic().bootstrapModule(AppModule);
