import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, Observable, of, tap } from 'rxjs';
import { SubjectsElement } from './subjects';

@Injectable({ providedIn: 'root' })
export class SubjectsService {

  private SubjectUrl = 'https://6320453be3bdd81d8ef655b0.mockapi.io/api/Subjects';
  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  constructor(private http: HttpClient) { }

  getList(): Observable<SubjectsElement[]> {
    return this.http.get<SubjectsElement[]>(this.SubjectUrl).pipe(
        tap(res =>console.log('tap:',res)),
        catchError(error => of([]))
    );
  }

  /** GET subject by id. Will 404 if id not found */
  getItemList(id: number): Observable<SubjectsElement> {
    const url = `${this.SubjectUrl}/${id}`;
    return this.http.get<SubjectsElement>(url)
  }

  /** POST: add a new subject to the server */
  addSubject(subject: SubjectsElement): Observable<any> {
    // console.log('service:',subject);

    return this.http.post<SubjectsElement>(this.SubjectUrl, subject, this.httpOptions)
  }


  /** PUT: update the subject on the server */
  updateSubject(subject: SubjectsElement): Observable<any> {
    console.log('subject:', subject.id);
    const url = `${this.SubjectUrl}/${subject.id}`;
    return this.http.put(url, subject, this.httpOptions)
  }

  /** DELETE: delete the subject from the server */
  deleteSubject(id: number): Observable<SubjectsElement> {
    const url = `${this.SubjectUrl}/${id}`;
    return this.http.delete<SubjectsElement>(url, this.httpOptions)
  }
}


