import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { SubjectsElement } from 'src/app/subjects';
import { SubjectsService } from 'src/app/subjects.service';
import { MatDialog } from '@angular/material/dialog';
import { DialogUpdateComponent } from '../dialog-update/dialog-update.component';
import { DialogDelateComponent } from '../dialog-delate/dialog-delate.component';

/**
 * @title Angular + Material - How To Refresh A Data Source (mat-table)
 */
@Component({
  selector: 'app-content',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.scss'],
})

export class ContentComponent implements OnInit {
  displayedColumns: string[] = ['id', 'nameCategory', 'codeCategory', 'support', 'status', 'note', 'action'];
  dataSource = new MatTableDataSource<SubjectsElement>([]);
  ListSubject: any;
  @ViewChild(MatPaginator, { static: true }) paginator !: MatPaginator;

  constructor(
    private myService: SubjectsService,
    public dialog: MatDialog
  ) { }

  ngOnInit() {
    this.refresh();
    this.dataSource.paginator = this.paginator;
  }

  refresh() {
    this.myService.getList().subscribe((data: SubjectsElement[]) => {
      this.dataSource.data = data;
    });
  }

  // Run evnent thêm danh mục môn học
  openDialogCreate(): void {
    const dialogRef = this.dialog.open(DialogUpdateComponent, {
      width: '1300px',
      data: { key: 'create', title: 'Thêm mới danh mục hồ sơ' }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      this.refresh();
    });
  }

  // Run evnent xoá danh mục môn học
  openDialogUpdate(id: number) {
    const dialogRef = this.dialog.open(DialogUpdateComponent, {
      width: '1300px',
      data: { key: 'update', userId: id, title: 'Chỉnh sửa danh mục hồ sơ' }
    });
    dialogRef.afterClosed().subscribe(result => {
      console.log('Đóng popup sẽ load lại table');
      this.refresh();
    });
  }

  // Run evnent xoá danh mục môn học
  openDialogDelete(subject: any): void {
    // this.loading = 0;
    this.ListSubject = Array(this.dataSource.data);
    this.ListSubject = this.ListSubject.filter((h: any) => h !== subject);
    this.myService.deleteSubject(subject).subscribe((res: any) => {
      console.log('đã xoá')
    });
    const dialogRef = this.dialog.open(DialogDelateComponent, {
      width: '200px',
      height: '180px',
    });
    dialogRef.afterClosed().subscribe(result => {
      this.refresh();
    });
  }
}




