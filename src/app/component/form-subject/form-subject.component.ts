import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { SubjectsElement } from 'src/app/subjects';
import { SubjectsService } from 'src/app/subjects.service';
import { DialogSucceedComponent } from '../dialog-succeed/dialog-succeed.component';
import { DialogUpdateComponent } from '../dialog-update/dialog-update.component';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-form-subject',
  templateUrl: './form-subject.component.html',
  styleUrls: ['./form-subject.component.scss']
})

export class FormSubjectComponent implements OnInit {
  FormSubject!: FormGroup;
  addSubject !: any;
  ItemSubject !: SubjectsElement;
  dataDialog: any;
  constructor(
    private fb: FormBuilder,
    private myService: SubjectsService,
    private DiaLog: DialogUpdateComponent,
    public dialog: MatDialog
  ) { }

  ngOnInit(): void {
    this.checkSubmit();
    let element = <any>document.getElementsByName("check-form");
    if (element.checked) {
      console.log(element.checked);
    }
  }

  checkSubmit() {
    this.dataDialog = this.DiaLog.status();
    if (this.dataDialog.key == 'update') {
      this.getItemList();
    }
    else {
      this.initForrm();
    }
  }

  getItemList(): void {
    this.myService.getItemList(this.dataDialog.id)
      .subscribe((ItemSubject: SubjectsElement) => {
        this.ItemSubject = ItemSubject;
        this.initForrm();
      });
  }

  initForrm() {
    this.FormSubject = this.fb.group({
      id: [this.ItemSubject ? this.ItemSubject.id : ''],
      nameCategory: [this.ItemSubject ? this.ItemSubject.nameCategory : '', Validators.required],
      codeCategory: [this.ItemSubject ? this.ItemSubject.codeCategory : '', Validators.required],
      support: [this.ItemSubject ? this.ItemSubject.support : '',],
      status: [this.ItemSubject ? (this.ItemSubject.status) ? true : false : false,],
      note: [this.ItemSubject ? this.ItemSubject.note : '',],
    });
  }

  add() {
    this.addSubject = this.FormSubject.value;
    this.myService.addSubject(this.addSubject).subscribe((response: any) => {
      console.log(response);
    });
  }

  // Update danh mục môn học
  update() {
    let dataInput = {
      id: this.FormSubject.value.id,
      nameCategory: this.FormSubject.value.nameCategory,
      codeCategory: this.FormSubject.value.codeCategory,
      support: this.FormSubject.value.support,
      status: this.FormSubject.value.status,
      note: this.FormSubject.value.note,
    }
    if (this.ItemSubject) {
      this.myService.updateSubject(dataInput).subscribe((res) => {

      }
      );
    }
  }

  // Mở diglog thông báo Succeed
  openDialogSucceed() {
    const dialogRef = this.dialog.open(DialogSucceedComponent, {
      width: '200px',
      height: '180px',
    });
    dialogRef.afterClosed().subscribe(result => {

    });
  }

  // Đóng form
  close() {
    this.DiaLog.onNoClick();
  }

  onSubmit() {
    if (this.dataDialog.key == 'update') {
      this.openDialogSucceed();
      this.update();
    }
    else {
      this.openDialogSucceed()
      this.add();
    }
  }
}
