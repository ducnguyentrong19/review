import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogDelateComponent } from './dialog-delate.component';

describe('DialogDelateComponent', () => {
  let component: DialogDelateComponent;
  let fixture: ComponentFixture<DialogDelateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DialogDelateComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(DialogDelateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
