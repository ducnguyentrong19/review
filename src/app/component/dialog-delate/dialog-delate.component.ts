import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Router } from '@angular/router';

export interface DialogData {
  animal: string;
  name: string;
}


@Component({
  selector: 'app-dialog-delate',
  templateUrl: './dialog-delate.component.html',
  styleUrls: ['./dialog-delate.component.scss']
})

export class DialogDelateComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<DialogDelateComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    ) { }

  ngOnInit(): void {
    setTimeout (() => {
      this.onNoClick();
   }, 2000);
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

}
