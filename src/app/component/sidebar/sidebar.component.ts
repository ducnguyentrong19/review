import {AfterViewInit, Component, ElementRef, OnInit, Renderer2, ViewChild, ViewChildren} from '@angular/core';


@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {

  constructor(
    private el: ElementRef,
    private renderer: Renderer2
  ) {
  }

  ngOnInit(): void {

  }
  toggleClass(event: any, className: string) {
    const hasClass = event.target.closest('.item-v1').classList.contains(className);
    if (hasClass) {
      this.renderer.removeClass(event.target.closest('.item-v1'), className);
    } else {
      this.renderer.addClass(event.target.closest('.item-v1'), className);
    }
  }
}
