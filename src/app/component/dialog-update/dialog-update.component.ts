import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

export interface DialogData {
  animal: string;
  name: string;
}

@Component({
  selector: 'app-dialog-update',
  templateUrl: './dialog-update.component.html',
  styleUrls: ['./dialog-update.component.scss']
})
export class DialogUpdateComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<DialogUpdateComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) { }
  dataFromModal: any = this.data;

  ngOnInit(): void {
  }
  status(){
      const ojb = {
        key: this.dataFromModal.key,
        id: this.dataFromModal.userId,
        title: this.dataFromModal.title
      }
      return ojb;
  }
  onNoClick(): void {
    this.dialogRef.close();
  }

}
