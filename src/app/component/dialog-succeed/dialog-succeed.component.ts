import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';

// export interface DialogData {
//   animal: string;
//   name: string;
// }

@Component({
  selector: 'app-dialog-succeed',
  templateUrl: './dialog-succeed.component.html',
  styleUrls: ['./dialog-succeed.component.scss']
})

export class DialogSucceedComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<DialogSucceedComponent>
    ) { }
  ngOnInit(): void {
    setTimeout (() => {
      this.onNoClick();
   }, 2000);
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

}

