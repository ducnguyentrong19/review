import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogSucceedComponent } from './dialog-succeed.component';

describe('DialogSucceedComponent', () => {
  let component: DialogSucceedComponent;
  let fixture: ComponentFixture<DialogSucceedComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DialogSucceedComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(DialogSucceedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
