export interface SubjectsElement {
  nameCategory: string;
  codeCategory: string;
  support: string;
  status: boolean;
  note: string;
  id: string;
}


