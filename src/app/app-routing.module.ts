import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ContentComponent } from './component/content/content.component';
import { DefaultComponent } from './component/default/default.component';
import { FooterComponent } from './component/footer/footer.component';
import { HeaderComponent } from './component/header/header.component';


const routes: Routes = [
  {
    path: 'home', component: DefaultComponent,
    children:[
      { path: 'table', component: ContentComponent },
      { path: 'hello', component: FooterComponent },
    ]
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
